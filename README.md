## Example scripts for [MilkyTrackerX](https://github.com/coderofsalvation/MilkyTrackerX)

<img src="howto.gif"/>

> Warning: this repository contains **example**-scripts (not turnkey solutions). However, it empowers you to extend [MilkyTrackerX](https://github.com/coderofsalvation/MilkyTrackerX) in a really **powerful** and **nerdy** way.

## Possibilities

* custom **SAMPLERS**: sample **ANY** synth/app/input into milkytracker!
* DIY '**plugin grabber**' by sampling desktop audio
* **generate** samples using puredata/csound etc
* **apply** a complex **effect** to a sample
* Extend the sampler with your **favorite SCRIPTING LANGUAGE(s)** 
* Create script-**pipelines between different tools**
* **automate** tedious tasks

> *samples ARE key-ingredients* for sample-trackers, so an unexplored territory is to **integrate** external tools with the sampler (and not block our creative flow by browsing Gigabytes of samples)

## Scripting Backends / Examples 

* **SOX**: (examples included by default) lots of [filters,timestretch etc](https://sox.sourceforge.net) but also synthesis ohmy!
* **FFMPEG**: (examples included by default) 
* **powershell**: see `random.sample.ps1` scripts (windows users only)
* **shellscript**: see `random.sample`  (mac/linux users only)
* **CSOUND**: run/generate synths/effects
* **PUREDATA**: run/generate synths/effects 
* **SUPERCOLLIDER**: (example needed) run/generate samples thru supercollider cli
* **CDP**: fascinating spectral processing using The Composers Desktop: [outdated examples here](https://gitlab.com/coderofsalvation/milkytracker-scripts-cdp)

## Howto add scripts

* Download the [ZIP](https://gitlab.com/coderofsalvation/milkytracker-scripts/-/archive/master/milkytracker-scripts-master.zip) or clone this repo
* right-click in the sample-editor > Script > Load`
* load `scripts.txt` (*) to populate the Script contextmenu (or directly run a scriptfile)
* profit!

> \* = `scripts.txt` is basically your (customizable) context-menu in the sample-editor. It allows you to easily switch between script-folders for collaboration/livecoding purposes (design samples in csound/puredata e.g.)

## Installation 

> WARNING: many examples will fail since they need to have certain software installed (!)

| Platform | Steps |
|-|-|
| **Linux** | $ sudo apt-get install sox ffmpeg csound pd gigtools  # (optional packages to run examples)  |
|           | $ sudo apt-get install jack_capture                   # (optional) if you prefer JACK over pulseaudio |
|           | |
| **Windows** | to run certain effects, certain files need to be installed **straigh into your** scripts-folder: |
|             | * [csound](https://github.com/csound/csound/releases/download/6.17.0/csound-6.17.0-windows-x64-binaries.zip) |
|             | * [csound plugins](https://github.com/csound/plugins/releases/download/1.0/Csound6-Plugins-Windows_x86_64-1.0.exe) |
|             | * [sox](https://gitlab.com/coderofsalvation/milkytracker-scripts/-/archive/sox/milkytracker-scripts-sox.zip) |
|             | * [puredata](https://msp.puredata.info/Software/pd-0.52-2.windows-installer.exe) |
|             | * ..and so on |
| **Mac OSX** | to run certain effects, certain files need to be installed **straigh into your** scripts-folder: |
|             | * [sox](https://gitlab.com/coderofsalvation/milkytracker-scripts/-/archive/sox/milkytracker-scripts-sox.zip) |
|             | * ..more feedback needed (maintainer does not own mac) |

> TIP: launch milkytracker from the terminal to allow cli-scripts requesting user input from the terminal.

## Sampling tips:

#### Most powerful: random-file-from-disk-sampler

**LINUX**: `random.sampler`, for example in your `scripts.txt` put:

```
SAMPLER random kick;./random.sample %s %s /samples/kickdrums /samples/pack/bassdrum|exec
SAMPLER random loop;SEARCH=loop ./random.sample %s %s /samples|exec
```

**WINDOWS**: `random.sampler.ps1`, for example in your `scripts.txt` put:

```
SAMPLER random kick;Powershell.exe -ExecutionPolicy Bypass -File random.sample.ps1 %s %s c:\\samples\kicks|exec
```



#### capturing desktop-audio (all audio-producing apps)

There are multiple ways:

* **LINUX**: 'Scripts > SAMPLER desktop [pulseaudio]' samples into `out.wav`
* **WINDOWS**: 'Scripts > SAMPLER desktop [stereomix]' samples into `out.wav` (*)

> \* = make sure to enable [stereo-mix](https://thegeekpage.com/stereo-mix/) to get this incredible feature to work.

> alternatively you can:

1. create a script-doorway to your favorite wave-editor (the wavosaur example in `scripts.txt` saves to `out.wav`) and record there.
2. let applications record to `render.wav` (script-folder) and import in milkytracker using `Script > import render.wav` (*)

> \* = or alternatively, duplicate that entry in `scripts.txt` and rename `render.wav` to `ableton.instruments.wav` for 'instrument-grabber'-exports.

## EXTERNAL SCRIPTS HOWTO

Milkytracker just expects an command in the config-file (see `Howto add scripts` above) and lines in this format:

```
<menu name>;<command formatstring>;<exec|filedialog_extension>
```
For example, take a look at `sox/pitch.up`:
```
sox/timestretch+;sox -D %s %s tempo 0.75 10 20 30|exec
```
> On linux this will run `sox -D /tmp/in.wav /tmp/out.wav tempo 0.75 10 20 30`

#### running a filedialog first

You can easily create a 'submenu' by changing `exec` to any extension (`csd` e.g.) which will trigger a filepicker:

```
csound/run patch;csound --nosound --strset1=%s --strset2=%s %s|csd
```

> This will open a filedialog (.csd extention only) to pick a file (`foo.csd` e.g.) and then runs `csound --nosound --strset1=/tmp/in.wav --streset2=/tmp/out.wav foo.csd`

After the script finishes, milkytracker will import `output.wav` (overwriting the current sample-slot).

> IMPORTANT: Make sure the command is be accessible for milkytracker (working-directory) or using `PATH`-environment variable

#### Scripting languages examples (shellscript/nodejs/python e.g.):

Just modify the shebang (#!) into your favorite scripting language:

```
#!/bin/bash
wavin="$1"
wavout="$2"
cp $wavin $wavout # ultra simple example
```

```
#!/usr/bin/env python3
import shutil
import sys
wavin  = sys.argv[1]
wavout = sys.argv[2]
shutil.copyfile(wavin,wavout)
```

```
#!/usr/bin/env node
let wavin  = process.argv[1];
let wavout = process.argv[2]; 
require('fs).copyFileSync( wavin, wavout );
```

> to call it check `SAMPLER pulseaudio` in `scripts.txt` as a reference (dont forget to `chmod +x yourscript`)

**on windows:**

```
$wavin=$args[0]     # input.wav
$wavout=$args[1]    # output.wav
Copy-Item $wavin -Destination $wavout
```

> Make sure the file name ends with `.ps1` (`myscript.ps1` e.g.) and call it like so: `Powershell.exe -ExecutionPolicy Bypass -File myscript.ps1 in.wav out.wav` (see windows samplers in `scripts.txt`)
