#!powershell.exe -File
$wavin=$args[0]     # input.wav
$wavout=$args[1]    # output.wav
$amp=20             # recording gain (update this when recording is too loud/soft)
$stereomix=0
if (Test-Path $wavout) {
	Remove-Item $wavout # fresh start
}

write-host "`nINPUT DEVICES:`n"
$devices = .\sox.exe -V6 -t waveaudio junkdevice junkfile.wav 2>&1 | ForEach-Object {
  if( $_ -match "Stereo Mix" ){ $stereomix=1; } 
  if ($_ -is [System.Management.Automation.ErrorRecord] -and $_ -match "Enumerating input") {
	$device = $_ -replace ".*Enumerating input device ",""
	$device = $device -replace "`"",""
    write-host $device
  }
}

if( $stereomix -match 0 ){ 
	write-host " ?: Stereo Mix is DISABLED (search online howto enable desktop-audio-recording using stereomix)"
}
$deviceIndex = Read-Host "`n> Enter recording device nr"

write-host "`n============= PRESS CTRL-C TO FINISH RECORDING ============`n"
try{ .\sox.exe '-v'$amp -t waveaudio $deviceIndex -b16 -r44100 $wavout }
catch{} # ignore ctrl-c exitcode
finally{ exit $LASTEXITCODE }
