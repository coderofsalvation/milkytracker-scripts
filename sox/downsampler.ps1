#!powershell.exe -File

$wavin=$args[0]     # input.wav

$wavout=$args[1]    # output.wav

$wavtmp=$wavin

$wavtmp+='.wav'  # temporary input.wav

#$amp=2              # recording gain (update this when recording is too loud/soft)


if (Test-Path $wavout) {

	Remove-Item $wavout # fresh start

}


$bits = 16

$hf   = 22050

$lf   = 30


$_bits = Read-Host -Prompt 'Output 16 or 8 bits? [16]'

$_hf   = Read-Host -Prompt 'highest freq?     [22050]'

$_lf   = Read-Host -Prompt 'lowest  freq?        [30]'

if ($_bits){ 

  $bits = $_bits

}

if ($_hf){ 

  $hf = $_hf

}

if ($_lf){ 

  $lf = $_lf

}

$rate = [int]$hf * 2

$hf = [int]$hf - 2 # no idea why sox complains about samplerate above nyquist (windows only)


..\..\bin\sox.exe $wavin --bits $bits --no-dither $wavout rate 44100 sinc $lf-$hf rate -a $rate

exit $LASTEXITCODE


