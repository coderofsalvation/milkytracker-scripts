#!powershell.exe -File
$wavin=$args[0]     # input.wav
$wavout=$args[1]    # output.wav
$amp=2              # recording gain (update this when recording is too loud/soft)
$stereomix=0
if (Test-Path $wavout) {
	Remove-Item $wavout # fresh start
}

$devices = .\sox.exe -V6 -t waveaudio junkdevice junkfile.wav 2>&1 | ForEach-Object {
  if ($_ -is [System.Management.Automation.ErrorRecord] -and $_ -match "Stereo Mix") {
	$stereomix=1  
	$device = $_ -replace ".*Enumerating input device ",""
	$deviceIndex = $device -replace ":.*",""
  }
}

if( $stereomix -match 0 ){ 
	write-host "[!] Stereo Mix is DISABLED :("
	write-host "[!] "
	write-host "[!] please enable stereomix-device in windows audiosettings."
	write-host "[!] once enabled it will allow you record audio-output from all applications `n"
	$foo = Read-Host "`n> Press key to quit`n"
	exit 1
}

write-host "`n============= PRESS CTRL-C TO FINISH RECORDING ============`n"
try{ .\sox.exe '-v'$amp -t waveaudio $deviceIndex -b16 -r44100 $wavout }
catch{} # ignore ctrl-c exitcode
finally{ exit $LASTEXITCODE }
