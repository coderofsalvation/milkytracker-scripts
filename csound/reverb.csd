<CsoundSynthesizer>
<CsOptions>
-odac -b2048
</CsOptions>
<CsInstruments>

; written for milkytracker by coderofsalvation/Leon van Kammen,2022
; example usage: csound --nosound --strset1=/tmp/in.wav --strset2=/tmp/out.wav reverb.csd

sr = 44100 
ksmps = 32 
0dbfs  = 1 
nchnls = 2
giRoom = 0.9
giDamp = 0.35
giWet  = 0.5;
giDry  = 0.9;
giOut  = 0.001;
gkDone = 0
gSin  strget 1                  ; input file
gSout strget 2                  ; output file
giLen filelen gSin              ; length file
giDuration = giLen +(4*giRoom)  ; add reverb tail
gkrecording = 0

FLpanel "Reverb",300,350 
  gkDry, giSliderDry  FLslider  "dry", 0, 1, 0 ,1, -1, 270,15, 15,20
  gkWet, giSliderWet  FLslider  "wet", 0, 1, 0 ,1, -1, 270,15, 15,60
  gkRoom,giSliderRoom FLslider  "roomsize", 0, 1, 0 ,1, -1, 270,15, 15,100
  gkDamp,giSliderDamp FLslider  "damp", 0, 1, 0 ,1, -1, 270,15, 15,140
  gkExit,ihExit FLbutton        "exit",1,  0,  21,  80,   25,  15, 315, 0, 98, 0, 0.001 
  gkApply,ihApply FLbutton      "apply", 1,  0,  21,  80,   25,  200, 315, 0, 99, 0, giDuration
  gkPreview,ihPreview FLbutton  "preview", 1,  0,  21,  80,   25,  110, 315, 0, 1, 0, giDuration
  FLsetVal_i giDry, giSliderDry
  FLsetVal_i giWet, giSliderWet
  FLsetVal_i giRoom, giSliderRoom
  FLsetVal_i giDamp, giSliderDamp
FLpanelEnd      
FLrun           

instr 1 ; play audio from disk
  ga1  diskin2  gSin, 1, 0, 0      ; play file from disk
  arevL, arevR freeverb ga1, ga1, gkRoom, gkDamp, sr, 0
  gaMix = (ga1*gkDry)+(arevL*gkWet)
  if (gkrecording != 1) then
    outs gaMix,gaMix
  endif
endin

instr 98
  exitnow
endin

instr 99
 if (gkrecording == 0) then 
   gkDone = p2+giDuration
   gkrecording = 1
   event "i", 1,           0,  giDuration ; play instr 1  (in.wav)
   event "i", 98, giDuration,        0.01 ; play instr 98 (exit) when done
 endif
 fout gSout,2,gaMix
endin

</CsInstruments>
<CsScore>
f 0 3600	;DUMMY SCORE EVENT ALLOWS REALTIME PLAYING FOR UP TO 1 HOUR
</CsScore>
</CsoundSynthesizer>
