#!powershell.exe -File
$wavin=$args[0]
$wavout=$args[1]
$folder=([Environment]::GetFolderPath("Desktop"))
$file=gci $folder/*.wav | where-object {$_.length -lt 100000} | random -c 1
try{
	write-host $file
	Copy-Item $file -Destination $wavout
	exit 0
}catch{
	exit 1
}

